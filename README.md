# InformationBundle

## Описание
Данный бандл предназначен для работы (вывода, обработки) с:

* Information - статьи
* InformationCategory - информационные категории
## InformationController
* informationCategoriesModuleAction - возвращает все статьи по всем активным категориям по текущему магазину
* informationCategoryPageAction - возвращает статьи по конкретной категории
* informationPageAction - возвращает конкретную статью

## Подключение
Для подключения данного модуля в проект необходимо:

* composer.json:

```json
{
    ...   
    "require": {
        ...
        "nitra/e-commerce-site-informationbundle": "dev-master",
        ...
    }
    ...
}
```

* app/AppKernel.php:

```php
<?php

    //...
use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    //...
    public function registerBundles()
    {
        //...
        $bundles = array(
            //...
            new Nitra\InformationBundle\NitraInformationBundle(),
            //...
        );
        //...
        return $bundles;
    }
    //...
}
```

* app/config/routing.yml:

```yaml
#...
nitra_information:
    resource: "@NitraInformationBundle/Controller/"
    type: annotation
#...
```