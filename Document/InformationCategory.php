<?php

namespace Nitra\InformationBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ODM\Document(collection="InformationCategories")
 */
class InformationCategory
{
    use \Gedmo\Blameable\Traits\BlameableDocument;
    use \Nitra\StoreBundle\Traits\AliasDocument;

    /**
     * @var string Идентификатор
     * @ODM\Id
     */
    protected $id;

    /**
     * @var string Название категории
     * @ODM\String
     * @Gedmo\Translatable
     * @Assert\NotBlank
     * @Assert\Length(max="255")
     */
    protected $name;

    /**
     * @var boolean Активность
     * @ODM\Boolean
     */
    protected $isActive;

    /**
     * @var boolean Отображать в модуле статей
     * @ODM\Boolean
     */
    protected $isArticlePage;

    /**
     * @var \Nitra\StoreBundle\Document\Store Магазин к которому относится
     * @ODM\ReferenceOne(targetDocument="Nitra\StoreBundle\Document\Store")
     */
    protected $store;

    /**
     * @var int Порядок сортировки
     * @ODM\Int
     */
    protected $sortOrder;

    /**
     * @var boolean Отображать в футере
     * @ODM\Boolean
     */
    protected $inFooter;

    /**
     * @var integer Порядок сортировки в футере
     * @ODM\Int
     */
    protected $sortOrderInFooter;

    /**
     * To string converter
     * @return string
     */
    public function __toString()
    {
        return (string) $this->name;
    }

    /**
     * Get id
     * @return string $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     * @param string $name
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set isActive
     * @param boolean $isActive
     * @return self
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
        return $this;
    }

    /**
     * Get isActive
     * @return boolean $isActive
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set store
     * @param \Nitra\StoreBundle\Document\Store $store
     * @return self
     */
    public function setStore($store)
    {
        $this->store = $store;
        return $this;
    }

    /**
     * Get store
     * @return \Nitra\StoreBundle\Document\Store $store
     */
    public function getStore()
    {
        return $this->store;
    }

    /**
     * Set sortOrder
     * @param int $sortOrder
     * @return self
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;
        return $this;
    }

    /**
     * Get sortOrder
     * @return int $sortOrder
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    /**
     * Set isArticlePage
     * @param boolean $isArticlePage
     * @return self
     */
    public function setIsArticlePage($isArticlePage)
    {
        $this->isArticlePage = $isArticlePage;
        return $this;
    }

    /**
     * Get isArticlePage
     * @return boolean $isArticlePage
     */
    public function getIsArticlePage()
    {
        return $this->isArticlePage;
    }

    /**
     * Set inFooter
     * @param boolean $inFooter
     * @return $this
     */
    public function setInFooter($inFooter)
    {
        $this->inFooter = $inFooter;
        return $this;
    }

    /**
     * Get inFooter
     * @return boolean
     */
    public function getInFooter()
    {
        return $this->inFooter;
    }

    /**
     * Set sortOrderInFooter
     * @param integer $sortOrderInFooter
     * @return $this
     */
    public function setSortOrderInFooter($sortOrderInFooter)
    {
        $this->sortOrderInFooter = $sortOrderInFooter;
        return $this;
    }

    /**
     * Get sortOrderInFooter
     * @return integer
     */
    public function getSortOrderInFooter()
    {
        return $this->sortOrderInFooter;
    }
}