<?php

namespace Nitra\InformationBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ODM\Document(collection="Informations", repositoryClass="Nitra\InformationBundle\Repository\InformationRepository")
 */
class Information
{
    use \Gedmo\Timestampable\Traits\TimestampableDocument;
    use \Gedmo\Blameable\Traits\BlameableDocument;
    use \Nitra\StoreBundle\Traits\AliasDocument;

    /**
     * @var string Идентификатор
     * @ODM\Id
     */
    protected $id;

    /**
     * @var string Заголовок
     * @ODM\String
     * @Assert\NotBlank
     * @Assert\Length(max = 255)
     * @Gedmo\Translatable
     */
    protected $title;

    /**
     * @var string Название
     * @ODM\String
     * @Assert\NotBlank
     * @Assert\Length(max = 255)
     * @Gedmo\Translatable
     */
    protected $name;

    /**
     * @var string Описание
     * @ODM\String
     * @Gedmo\Translatable
     */
    protected $description;

    /**
     * @var string Краткое описание
     * @ODM\String
     * @Gedmo\Translatable
     */
    protected $shortDescription;

    /**
     * @var boolean Активность
     * @ODM\Boolean
     */
    protected $isActive;

    /**
     * @var \Nitra\InformationBundle\Document\InformationCategory Категория статей
     * @ODM\ReferenceOne(targetDocument="InformationCategory")
     */
    protected $informationCategory;

    /**
     * @var string CSS класс для стилизации
     * @ODM\String
     * @Gedmo\Translatable
     */
    protected $cssClass;

    /**
     * @var int Порядок сортировки
     * @ODM\Int
     */
    protected $sortOrder;

    /**
     * @var string Путь к изображению
     * @ODM\String
     */
    protected $image;

    /**
     * @var \DateTime Дата публикации
     * @ODM\Date
     */
    protected $publishedAt;

    /**
     * @var \Nitra\SeoBundle\Document\EmbedSeo SEO описание
     * @ODM\EmbedOne(targetDocument="Nitra\SeoBundle\Document\EmbedSeo")
     */
    protected $seoInfo;

    /**
     * To string converter
     * @return string
     */
    public function __toString()
    {
        return (string) $this->name;
    }

    /**
     * Get id
     * @return string $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     * @param string $title
     * @return self
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * Get title
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set name
     * @param string $name
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     * @param string $description
     * @return self
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * Get description
     * @return string $description
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set cssClass
     * @param string $cssClass
     * @return self
     */
    public function setCssClass($cssClass)
    {
        $this->cssClass = $cssClass;
        return $this;
    }

    /**
     * Get cssClass
     * @return string $cssClass
     */
    public function getCssClass()
    {
        return $this->cssClass;
    }

    /**
     * Set sortOrder
     * @param int $sortOrder
     * @return self
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;
        return $this;
    }

    /**
     * Get sortOrder
     * @return int $sortOrder
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    /**
     * Set isActive
     * @param boolean $isActive
     * @return self
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
        return $this;
    }

    /**
     * Get isActive
     * @return boolean $isActive
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set informationCategory
     * @param \Nitra\InformationBundle\Document\InformationCategory $informationCategory
     * @return self
     */
    public function setInformationCategory($informationCategory)
    {
        $this->informationCategory = $informationCategory;
        return $this;
    }

    /**
     * Get informationCategory
     * @return \Nitra\InformationBundle\Document\InformationCategory $informationCategory
     */
    public function getInformationCategory()
    {
        return $this->informationCategory;
    }

    /**
     * Set image
     * @param string $image
     * @return self
     */
    public function setImage($image)
    {
        $this->image = $image;
        return $this;
    }

    /**
     * Get image
     * @return string $image
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set shortDescription
     * @param string $shortDescription
     * @return self
     */
    public function setShortDescription($shortDescription)
    {
        $this->shortDescription = $shortDescription;
        return $this;
    }

    /**
     * Get shortDescription
     * @return string $shortDescription
     */
    public function getShortDescription()
    {
        return $this->shortDescription;
    }

    /**
     * Set seoInfo
     * @param \Nitra\SeoBundle\Document\EmbedSeo $seoInfo
     * @return self
     */
    public function setSeoInfo($seoInfo)
    {
        $this->seoInfo = $seoInfo;
        return $this;
    }

    /**
     * Get seoInfo
     * @return \Nitra\SeoBundle\Document\EmbedSeo $seoInfo
     */
    public function getSeoInfo()
    {
        return $this->seoInfo;
    }

    /**
     * Set publishetAt
     * @param \DateTime $publishedAt
     * @return self
     */
    public function setPublishedAt(\DateTime $publishedAt)
    {
        $this->publishedAt = $publishedAt;

        return $this;
    }

    /**
     * Get publishedAt.
     * @return \DateTime
     */
    public function getPublishedAt()
    {
        return $this->publishedAt;
    }
}