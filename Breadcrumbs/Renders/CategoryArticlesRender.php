<?php

namespace Nitra\InformationBundle\Breadcrumbs\Renders;

use Nitra\ProductBundle\Breadcrumbs\AbstractRender;
use Nitra\ProductBundle\Breadcrumbs\BreadcrumbItem;

class CategoryArticlesRender extends AbstractRender
{
    /**
     * {@inheritdoc}
     */
    public function getBreadcrumbs($object)
    {
        $append = array();

        if ($object instanceof \Nitra\InformationBundle\Document\InformationCategory) {
            $category = $object;
        } elseif ($object instanceof \Nitra\InformationBundle\Document\Information) {
            $category = $object->getInformationCategory();
            $append[] = new BreadcrumbItem($object, 'information_page', array(
                'slug' => $object->getAlias(),
            ));
        }

        $items = array(
            new BreadcrumbItem($category, 'information_category_page', array(
                'slug' => $category->getAlias(),
            )),
        );

        return array_merge($items, $append);
    }

    /**
     * {@inheritdoc}
     */
    public function getSupportClass()
    {
        return array(
            '\Nitra\InformationBundle\Document\InformationCategory',
            '\Nitra\InformationBundle\Document\Information',
        );
    }
}