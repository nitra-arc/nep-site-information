<?php

namespace Nitra\InformationBundle\Controller\Information;

use Nitra\StoreBundle\Controller\NitraController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Nitra\StoreBundle\Lib\Globals;

class InformationController extends NitraController
{
    /**
     * @return \Doctrine\ODM\MongoDB\DocumentRepository
     */
    protected function getCategoryRepository()
    {
        return $this->getDocumentManager()->getRepository('NitraInformationBundle:InformationCategory');
    }

    /**
     * @return \Nitra\InformationBundle\Repository\InformationRepository
     */
    protected function getArticlesRepository()
    {
        return $this->getDocumentManager()->getRepository('NitraInformationBundle:Information');
    }

    /**
     * @Route("/information-page/{slug}", name="information_page", defaults={"slug" = ""})
     * @Template("NitraInformationBundle:Information:informationPage.html.twig")
     * @Cache(maxage="3600", public="true")
     *
     * @param $slug
     *
     * @return array
     */
    public function informationPageAction($slug)
    {
        // Текущий магазин
        $store = Globals::getStore();
        // формирования алиаса для поиска в базе данных
        $urlTrans = (isset($store['urlTrans'])) ? $store['urlTrans'] : 'en';

        $information = $this->getArticlesRepository()
            ->getDefaultQb()
            ->field('alias' . ucfirst($urlTrans))->equals(mb_strtolower($slug, 'UTF-8'))
            ->getQuery()->execute()->getSingleResult();

        if (!$information) {
            throw $this->createNotFoundException('Статья не найдена');
        }

        return array(
            'information' => $information,
        );
    }

    /**
     * @Route("/information-category-page/{slug}", name="information_category_page", defaults={"slug" = ""})
     * @Template("NitraInformationBundle:Information:informationCategoryPage.html.twig")
     * @Cache(maxage="3600", public="true")
     *
     * @param string $slug
     * @param string $sort
     * @param string $order
     *
     * @return array
     */
    public function informationCategoryPageAction($slug, $sort = 'publishedAt', $order = 'desc')
    {
        $category = $this->getCategoryRepository()
            ->createQueryBuilder()
            ->field('aliasEn')->equals(mb_strtolower($slug, 'UTF-8'))
            ->getQuery()->execute()->getSingleResult();

        if (!$category) {
            throw $this->createNotFoundException('Articles category not found');
        }

        $query = $this->getArticlesRepository()
            ->getDefaultQb()
            ->field('informationCategory.id')->equals($category->getId())
            ->sort($sort, $order)
            ->getQuery();

        if (!$query->count()) {
            throw $this->createNotFoundException('Articles not found');
        }

        // pagination
        $articles = $this->paginate($query, 'information');

        return array(
            'informations' => $articles,
            'category'     => $category,
        );
    }

    /**
     * Модуль информационных категорий и их статей
     *
     * @Route(name="information_categories_module")
     * @Template("NitraInformationBundle:Information:informationCategoriesModule.html.twig")
     *
     * @return array
     */
    public function informationCategoriesModuleAction()
    {
        //получаем информационные категории текущего магазина
        $categories = $this->getCategoryRepository()
            ->createQueryBuilder()
            ->field('isArticlePage')->equals(true)
            ->sort('sortOrder', 'asc')
            ->getQuery()->execute();

        $informationCategories = array();
        $articlesLimit = Globals::getStoreLimit('information_module', 3);
        foreach ($categories as $category) {
            $articles = $this->getArticlesRepository()
                ->getDefaultQb()
                ->field('informationCategory.id')->equals($category->getId())
                ->sort('sortOrder', 'asc')
                ->limit($articlesLimit)
                ->getQuery()->execute();

            if ($articles->count()) {
                $informationCategories[] = array(
                    'category' => $category,
                    'articles' => $articles,
                );
            }
        }

        return array(
            'categories' => $informationCategories,
        );
    }

    /**
     * @Template("NitraInformationBundle:Information:informationCategoryModule.html.twig")
     *
     * @param string    $id
     * @param integer   $limit
     * @param string    $imageFilter
     *
     * @return array
     *
     * @throws \Doctrine\ODM\MongoDB\LockException
     */
    public function informationCategoryModuleAction($id, $limit = -1, $imageFilter = 'information_small')
    {
        $category = $this->getCategoryRepository()->find($id);

        if (!$category) {
            throw $this->createNotFoundException(sprintf("Articles category by id \"%s\" not found", $id));
        }

        $qb = $this->getArticlesRepository()
            ->getDefaultQb()
            ->field('informationCategory.id')->equals($category->getId())
            ->sort('sortOrder', 'asc');

        if ($limit > 0) {
            $qb->limit($limit);
        }

        return array(
            'articles'      => $qb->getQuery()->execute(),
            'imageFilter'   => $imageFilter,
        );
    }

    /**
     * @Template("NitraInformationBundle:Information:informationMenu.html.twig")
     *
     * @param string        $location   Location
     * @param null|string   $current    Current article id
     *
     * @return array
     */
    public function informationMenuAction($location = 'header', $current = null)
    {
        $store     = $this->getStore();
        $articles  = array();
        $canonical = implode('', array_map('ucfirst', explode('_', $location)));

        if (array_key_exists('informationMenuCategory', $store) && $store['informationMenuCategory']) {
            $method = 'informationMenu' . $canonical;
            if (!method_exists($this, $method)) {
                $method = 'informationMenuDefault';
            }
            $articles = $this
                ->$method($store['informationMenuCategory'])
                ->getQuery()->execute();
        }

        $context = array(
            'informations' => $articles,
            'location'     => $location,
            'current'      => $current,
        );

        if (method_exists($this, $method = 'get' . $canonical . 'Categories')) {
            $context['categories'] = $this->$method();
        }

        return $context;
    }

    /**
     * Default articles getter for menu
     *
     * @param string $informationCategoryId
     *
     * @return \Doctrine\DBAL\Query\QueryBuilder
     */
    protected function informationMenuDefault($informationCategoryId)
    {
        return $this->getArticlesRepository()
            ->getDefaultQb()
            ->field('informationCategory.id')->equals($informationCategoryId)
            ->sort('sortOrder', 'asc');
    }

    /**
     * @return \Nitra\InformationBundle\Document\InformationCategory[]
     */
    protected function getFooterCategories()
    {
        return $this->getCategoryRepository()
            ->createQueryBuilder()
            ->field('inFooter')->equals(true)
            ->sort('sortOrderInFooter', 1)
            ->getQuery()->execute();
    }
}