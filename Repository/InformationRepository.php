<?php

namespace Nitra\InformationBundle\Repository;

use Doctrine\ODM\MongoDB\DocumentRepository;

class InformationRepository extends DocumentRepository
{
    /**
     * getting category repository
     * @return \Nitra\InformationBundle\Repository\InformationCategoryRepository
     */
    protected function getCategoryRepository()
    {
        return $this->dm->getRepository('NitraInformationBundle:InformationCategory');
    }

    /**
     * getting default conditions for articles
     * @return \Doctrine\MongoDB\Query\Builder
     */
    public function getDefaultQb()
    {
        $categoriesIds = $this->getCategoryRepository()
            ->createQueryBuilder()
            ->distinct('_id')
            ->getQuery()->execute()->toArray();

        $qb = $this->createQueryBuilder()
            ->field('informationCategory.id')->in($categoriesIds);

        return $qb;
    }
}